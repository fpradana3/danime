import React, { useEffect, useState } from "react";
import { render } from "react-dom";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider
} from "@apollo/client";
import {AnimeList} from './components/AnimeList/AnimeList'
import { AnimeDetail } from "./components/AnimeDetail/AnimeDetail";
import { MyCollections } from "./components/MyCollections/MyCollections";
import { CollectionDetail } from "./components/CollectionDetail/CollectionDetail";
import circleBack from './assets/circleback.svg'
import './index.css';

const client = new ApolloClient({
  uri: "https://graphql.anilist.co",
  cache: new InMemoryCache()
});

function App() {
  const [page,setPage] = useState(1);
  const [posisi, setPosisi] = useState('list')
  const [data, setData] = useState()
  const [coldilihat, setColdilihat] = useState('')

  useEffect(()=>{
    if((localStorage.getItem("MyCollections")===null) || (localStorage.getItem("MyCollections")==='')){
      localStorage.setItem("MyCollections",JSON.stringify([]))
    }
  },[])

  useEffect(()=>{
    window.scrollTo(0, 0)
  },[posisi])


  return (
    <div>
      
        {posisi==="list" && (
          <div>
            <p className="WebTitle"><span className="yellow">DA</span>nime</p>
            <div className="flex center">
              <p className="Bar Underline">Anime List</p>
              <p className="Bar" onClick={()=>setPosisi("mycol")}>My Collections</p>
            </div>
          </div>
        )}

        {posisi==="mycol" && (
          <div>
          <p className="WebTitle"><span className="yellow">DA</span>nime</p>
            <div className="flex center">
              <p className="Bar" onClick={()=>setPosisi("list")}>Anime List</p>
              <p className="Bar Underline">My Collections</p>
            </div>
          </div>
        )}
        
      {
        posisi==="list" && (
          <div>
            <div className="flex konten">
              {
                page>1 && (
                  <button className="buttonmini" onClick={()=>setPage(page-1)}>Prev 10</button>
                )
              }
              <button className="marginL2 buttonmini" onClick={()=>setPage(page+1)}>Next 10</button>
            </div>
            <AnimeList page={page} lihatDetail={()=>{setPosisi('detail');}} simpanData={e=>setData(e)}/>
          </div>
        )
      }
      
      {
        posisi==="mycol" && (
        <div>
          <MyCollections fungsi1={()=>setPosisi('Coldet')} fungsi2={e=>setColdilihat(e)}/>
        </div>
      )}

      {
        posisi==="Coldet" && (
          <div>
            <img alt="icon" onClick={()=>setPosisi("mycol")} src={circleBack} className="back"></img>
            <CollectionDetail fungsi1={()=>setPosisi('detail2')} fungsi2={e=>setData(e)} namaKoleksi={coldilihat} fungsi3={e=>{setColdilihat(e);}} />
          </div>
        )
      }

      {
        posisi==="Coldet2" && (
          <div>
            <img alt="icon" onClick={()=>setPosisi("detail2")} src={circleBack} className="back"></img>
            <CollectionDetail fungsi1={()=>setPosisi('detail2')} fungsi2={e=>setData(e)} namaKoleksi={coldilihat} fungsi3={e=>{setColdilihat(e);}} />
          </div>
        )
      }

      {
        posisi==="detail" && (
          <div>
            <img alt="icon" onClick={()=>setPosisi("list")} src={circleBack} className="back"></img>
            <AnimeDetail data={data} fungsi={()=>setPosisi('Coldet2')} fungsi2={e=>setColdilihat(e)}/>
          </div>
        )
      }

    {
        posisi==="detail2" && (
          <div>
            <img alt="icon" onClick={()=>setPosisi("Coldet")} src={circleBack} className="back"></img>
            <AnimeDetail data={data} fungsi={()=>setPosisi('Coldet2')} fungsi2={e=>setColdilihat(e)}/>
          </div>
        )
      }
    </div>
  );
}

render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);
