import React, { useEffect, useState } from "react";
import { Content } from './styles';
import defaultCover from '../../assets/defaultCover.png'
import trash from '../../assets/trash.svg'
import edit from '../../assets/edit.svg'

export const MyCollections = ({fungsi1, fungsi2}) =>{
    const [showAddTo, setShowAddTo] = useState(false)
    const [animeCol, setAnimeCol] = useState([])
    const [colDiedit, setColDiedit] = useState('')
    const [showEditCol, setShowEditCol] = useState(false)
    const [showremove, setShowremove] = useState(false)
    const [untukDihapus, setUntukDihapus] = useState('')
    useEffect(()=>{
       setAnimeCol(JSON.parse(localStorage.getItem("MyCollections")))
    },[])
    const [nama,setNama] = useState("")
    const [val1, setval1] = useState(false)
    const [val2, setval2] = useState(false)
    const [val3, setval3] = useState(false)
    const [val4,setval4] = useState(false)

    const reset = () =>{
        setNama("")
        setval1(false)
        setval2(false)
        setval3(false)
        setval4(false)
    }

    const removeCollection = async (e) => {
        var array2 = JSON.parse(localStorage.getItem("MyCollections"))
        var index = array2.indexOf(e);
        array2.splice(index, 1);
        setAnimeCol(array2)
        localStorage.setItem("MyCollections",JSON.stringify(array2))
        var untukDihapus = JSON.parse(localStorage.getItem(e))
        for(let i=0;i<untukDihapus.length;i++){
            var arraynime = JSON.parse(localStorage.getItem(untukDihapus[i].title.romaji))
            var index2 = arraynime.indexOf(e);
            arraynime.splice(index2, 1)
            localStorage.setItem(untukDihapus[i].title.romaji,JSON.stringify(arraynime))
        }
        setShowremove(false)
        localStorage.removeItem(e)
    }

    const changeCollectionName = async (namanya) => {
        if(namanya===""){
            setval3(true)
            return
        }else{
            setval3(false)
        }
        if((namanya.replace(/\s/g,'')).replace(/^[a-z\d.]*$/i,"")!==""){
            setval2(true)
            return
        }else{
            setval2(false)
        }if(namanya===colDiedit){
            setval4(true)
            return
        }else{
            setval4(false)
        }
        var isi = JSON.parse(localStorage.getItem(namanya))
        if(isi===null){
            isi = JSON.parse(localStorage.getItem(colDiedit))
            localStorage.setItem(namanya,JSON.stringify(isi))
            var array2 = JSON.parse(localStorage.getItem("MyCollections"))
            var index = array2.indexOf(colDiedit);
            array2.splice(index, 1);
            array2.unshift(namanya)
            setAnimeCol(array2)
            localStorage.setItem("MyCollections",JSON.stringify(array2))
            localStorage.removeItem(colDiedit)
            for(let i=0;i<isi.length;i++){
                var arraynime = JSON.parse(localStorage.getItem(isi[i].title.romaji))
                var index2 = arraynime.indexOf(colDiedit);
                arraynime.splice(index2, 1)
                arraynime.unshift(namanya)
                localStorage.setItem(isi[i].title.romaji,JSON.stringify(arraynime))
            }
            
            setShowEditCol(false)
            reset()
        }else{
            setval1(true)
        }
    }

    const addNewCollection = async (namanya) => {
        if(namanya===""){
            setval3(true)
            return
        }else{
            setval3(false)
        }
        if((namanya.replace(/\s/g,'')).replace(/^[a-z\d.]*$/i,"")!==""){
            setval2(true)
            return
        }else{
            setval2(false)
        }
        var isi = JSON.parse(localStorage.getItem(namanya))
        if(isi===null){
            isi = []
            localStorage.setItem(namanya,JSON.stringify(isi))
            animeCol.unshift(namanya)
            localStorage.setItem("MyCollections",JSON.stringify(animeCol))
            setShowAddTo(false)
            reset()
        }else{
            setval1(true)
        }
        
    }


    return(
        <Content>
            <div className="Container">
            <button onClick={()=>setShowAddTo(true)} className="button">Add New Collection</button>
                
                <div>
                {
                    animeCol.length===0?
                    <div>
                        <p className="textkosong">data kosong</p>
                    </div>
                    :
                    <div className="contentwrap">
                        {
                            animeCol.length!==0 && (
                            animeCol.map((post)=>(
                                <div onClick={e=>{e.stopPropagation();fungsi1();fungsi2(post)}} className="flex marginbot">
                                    {
                                        JSON.parse(localStorage.getItem(post)).length===0 ? 
                                        <img className="FotoCol" alt="foto" src={defaultCover}/>
                                        :
                                        <img className="FotoCol" alt="foto" src={JSON.parse(localStorage.getItem(post))[0].coverImage.extraLarge}/>
                                    }
                                    <p className="DeskripsiCol">{post}</p>
                                    <p className="DeskripsiCol2">{JSON.parse(localStorage.getItem(post)).length} Anime</p>
                                    <div className="center column">
                                        <img onClick={e=>{e.preventDefault();e.stopPropagation();setColDiedit(post);setShowEditCol(true)}} alt="icon" src={edit} className='trash'></img>
                                        <img onClick={e=>{e.preventDefault();e.stopPropagation();setUntukDihapus(post);setShowremove(true)}} alt="icon" className="center trash" src={trash} />
                                    </div>
                                </div>
                            )))
                        }
                    </div>
                }
                </div>
                

                {
                    showAddTo && (
                        <div>
                            <div onClick={()=>{setShowAddTo(false);reset()}} className="outside"></div>
                            <div className="Modal">
                                <p className="flex center AnimeTag Margin0">Add New Collection</p>
                                <p className="Section Margin0">Collection Name</p>
                                <input type="text" value={nama} onChange={e => setNama(e.target.value)} className='form' placeholder="ex: Favorite List"></input>
                                {
                                    val1 && (
                                        <p className="error">Collection Existed</p>
                                    )
                                }
                                {
                                    val2 && (
                                        <p className="error">Cannot Use Special Character</p>
                                    )
                                }
                                {
                                    val3 && (
                                        <p className="error">Name cannot be empty</p>
                                    )
                                }
                                <button onClick={()=>addNewCollection(nama)} className="button margint">Add</button>
                            </div>
                        </div>
                    )
                }

                {
                    showEditCol && (
                        <div>
                            <div onClick={()=>{setShowEditCol(false);reset()}} className="outside"></div>
                            <div className="Modal">
                                <p className="flex center AnimeTag Margin0">Change Collection Name</p>
                                <p className="Section Margin0">Collection Name</p>
                                <input type="text" value={nama} onChange={e => setNama(e.target.value)} className='form' placeholder="ex: Favorite List"></input>
                                {
                                    val1 && (
                                        <p className="error">Collection Existed</p>
                                    )
                                }
                                {
                                    val2 && (
                                        <p className="error">Cannot Use Special Character</p>
                                    )
                                }
                                {
                                    val3 && (
                                        <p className="error">Name cannot be empty</p>
                                    )
                                }
                                 {
                                    val4 && (
                                        <p className="error">Name cannot be same</p>
                                    )
                                }
                                <button onClick={()=>changeCollectionName(nama)} className="button margint">Confirm</button>
                            </div>
                        </div>
                    )
                }

                {
                    showremove && (
                        <div>
                            <div onClick={()=>setShowremove(false)} className="outside"></div>
                            <div className="Modal">
                                <p className="flex center AnimeTag Margin0">Remove Confirmation</p>
                                <p className="Section centext center Margin0">Anda yakin ingin menghapus anime ini dari koleksi anda?</p>
                                <button onClick={()=>removeCollection(untukDihapus)} className="button margint">Hapus</button>
                            </div>
                        </div>
                    )
                }
            </div>
        </Content>
    )
}