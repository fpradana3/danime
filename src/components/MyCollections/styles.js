import styled from '@emotion/styled';

export const Content = styled.div`
@media screen {
    word-break: break-word; 

    .Container{
        background-color:#212121;
        padding-bottom:20vh;
    }

    .flex{
        display:flex
    }

    .contentwrap{
        margin-top:20px;
        width:60%;
        margin-left:auto;
        margin-right:auto;
    }

    .button{
        background-color: #FCCC24;
        border: none;
        font-weight:bold;
        padding: 15px;
        width: 60%;
        margin-top:16px;
        margin-bottom: 36px;
        cursor: pointer;
        display: block;
        border-radius: 15px;
        margin-left: auto;
        margin-right: auto;
    }

    .marginbot{
        margin-bottom:32px;
        cursor:pointer
    }

    .FotoCol{
        flex:20%;
        width:20%;
    }

    .DeskripsiCol{
        margin:0;
        flex:55%;
        text-align:center;
        margin-left:16px;
        margin-right:16px;
        transition: color 0.3s;
        font-size:24px;
    }

    .marginbot:hover .DeskripsiCol{
        color:#FCCC24;
    }

    .centext{
        text-align:center;
    }

    .marginbot:hover .DeskripsiCol2{
        color:#FCCC24;
    }

    .center{
        margin-left:auto;
        margin-right:auto;
        justify-content:center;
    }

    .column{
        display:flex;
        flex-direction:column;
    }

    .trash{
        width:42px;
        margin-top:8px;
        margin-bottom:8px;
        margin-left:8px;
        cursor:pointer;
    }

    .DeskripsiCol2{
        margin:0;
        flex:35%;
        transition: color 0.3s;
        text-align:center;
        margin-left:16px;
        margin-right:16px;
        font-size:24px;
    }

    .AnimeImage{
        width:20%;
        height: max-content;
    }

    .DetailAnime{
        flex:70%;
        padding-left:32px;
    }

    .AnimeTag{
        font-weight:bold;
        font-size:28px;
        color: #166DC4;
    }

    .outside{
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        opacity: 0.6;
        align-items: center;
        justify-content: center;
        height: 100vh;
        width: 100vw;
        z-index: 100;
    }

    .Modal{
        position: fixed;
        min-width: 60vw;
        z-index: 120;
        background-color: white;
        top: 50%;
        border-radius: 15px;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 5vh 5vw 5vh 5vw;
        max-height: 70vh;
        color:black;
        overflow: scroll;
    }

    .Modal::-webkit-scrollbar{
        width:0;
        height:0;
        scrollbar-width: none;
    }

    .Margin0{
        margin:0
    }

    .Section{
        font-size:24px;
        cursor:pointer;
        margin-top:24px;
    }

    .margint{
        margin-top:24px;
    }

    .textkosong{
        margin-top:64px;
        text-align:center;
    }

    .error{
        color:#FF7D7D;
        text-align:right;
        margin:0;
    }

    .form{
        width: -webkit-fill-available;
        width: -moz-available;
        padding: 15px;
        border-radius: 15px;
        outline: none;
        margin-top:8px;
        margin-bottom: 8px;
        border: 1px solid #6F6F6F;
        overflow: hidden;  
        text-overflow:ellipsis;
        white-space: nowrap;
    }
}

@media screen and (max-width: 1000px) {
    .AnimeImage{
        width:30%
    }

    .trash{
        width:24px;
    }

    .form{
        padding:12px;
    }

    .DeskripsiCol{
        font-size:16px;
    }

    .DeskripsiCol2{
        font-size:16px;
    }

    .Section{
        font-size:16px;
    }

    .DetailAnime{
        padding-left:16px;
    }

    .AnimeTag{
        font-size:21px;
    }

    .contentwrap{
        width:88%;
    }
} 
`;
