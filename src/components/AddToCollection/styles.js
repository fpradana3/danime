import styled from '@emotion/styled';

export const Content = styled.div`
@media screen {
    word-break: break-word; 

    .Container{
        background-color:#212121;
        padding-bottom:20vh;
    }

    .outside{
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        opacity: 0.6;
        align-items: center;
        justify-content: center;
        height: 100vh;
        width: 100vw;
        z-index: 100;
    }

    .Modal{
        position: fixed;
        min-width: 60vw;
        z-index: 120;
        background-color: white;
        top: 50%;
        border-radius: 15px;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 5vh 5vw 5vh 5vw;
        max-height: 70vh;
        color:black;
        overflow: scroll;
    }

    .Modal::-webkit-scrollbar{
        width:0;
        height:0;
        scrollbar-width: none;
    }

    .marginL{
        margin-left:16px;
    }

    .flex{
        display:flex;
    }

    .center{
        margin-left:auto;
        margin-right:auto;
        justify-content:center;
    }

    .AnimeTag{
        font-weight:bold;
        font-size:28px;
        color: #166DC4;
    }

    .Margin0{
        margin:0
    }

    .Underline{
        border-bottom: 2px solid #FCCC24;
        padding-bottom: 4px;
    }

    .Section{
        font-size:24px;
        cursor:pointer;
    }

    .form{
        width: -webkit-fill-available;
        width: -moz-available;
        padding: 15px;
        border-radius: 15px;
        outline: none;
        margin-top:8px;
        margin-bottom: 8px;
        border: 1px solid #6F6F6F;
        overflow: hidden;  
        text-overflow:ellipsis;
        white-space: nowrap;
    }

    .error{
        color:#FF7D7D;
        text-align:right;
        margin:0;
    }

    .button{
        background-color: #FCCC24;
        border: none;
        font-weight:bold;
        padding: 15px;
        width: 60%;
        margin-top: 24px;
        cursor: pointer;
        display: block;
        border-radius: 15px;
        margin-left: auto;
        margin-right: auto;
    }

    .margin0{
        margin:0;
    }

    .collectionWrapper{
        flex-wrap: wrap;
        margin-top:32px;
        max-height: 30vh;
        overflow-y: scroll;
    }

    .collectionWrapper::-webkit-scrollbar {
        width: 6px;
        height:6px;
        background-color: #F5F5F5;
    }
    
    .collectionWrapper::-webkit-scrollbar-thumb {
        background-color: #000000;
        border-radius: 4px;
    }
    
    .collectionWrapper::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
        border-radius: 4px;
    }

    .formWrapper{
        flex:33.33%
    }

    .break{
        word-break: break-word; 
    }
}

@media screen and (max-width: 1000px) {
    .AnimeTag{
        font-size:21px;
    }

    .Section{
        font-size:16px;
    }

    .form{
        padding:12px;
    }

    .collectionWrapper{
        margin-top:12px;
    }

    .formWrapper{
        flex:50%
    }
} 
`;
