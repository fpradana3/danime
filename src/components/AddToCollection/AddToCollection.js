import React, { useState } from "react";
import { Content } from './styles';

export const AddToCollection = ({fungsi, data, animeCol}) =>{
    const [status,setStatus] = useState("Old")
    const [nama,setNama] = useState("")
    const [val1, setval1] = useState(false)
    const [val2, setval2] = useState(false)
    const [val3, setval3] = useState(false)
    const [val4, setval4] = useState(false)
    const selected = []
    const Collection = JSON.parse(localStorage.getItem("MyCollections"))

    const handleSelectedChange = (e) =>{
        var index = selected.indexOf(e);

        if (index > -1) {
            selected.splice(index, 1);
        }else{
            selected.push(e)
        }
    }

    const addToExistingCollection = async () => {
        if(selected.length>0){
            for(let i=0;i<selected.length;i++){
                var isi = JSON.parse(localStorage.getItem(selected[i]))
                isi.push(data)
                localStorage.setItem(selected[i],JSON.stringify(isi))
                animeCol.push(selected[i])
                localStorage.setItem(data.title.romaji,JSON.stringify(animeCol))
            }
            fungsi()
        }else{
            setval3(true)
        }
    }
    

    const addNewCollection = async (namanya) => {
        if(namanya===""){
            setval4(true)
            return
        }else{
            setval4(false)
        }
        if((namanya.replace(/\s/g,'')).replace(/^[a-z\d.]*$/i,"")!==""){
            setval2(true)
            return
        }else{
            setval2(false)
        }
        var isi = JSON.parse(localStorage.getItem(namanya))
        if(isi===null){
            isi = [data]
            localStorage.setItem(namanya,JSON.stringify(isi))
            Collection.push(namanya)
            localStorage.setItem("MyCollections",JSON.stringify(Collection))
            animeCol.push(namanya)
            localStorage.setItem(data.title.romaji,JSON.stringify(animeCol))
            fungsi()
        }else{
            setval1(true)
        }
        
    }

    return(
        <Content>
            <div className="Container">
                <div onClick={()=>fungsi()} className="outside"></div>
                <div className="Modal">
                    <p className="flex center AnimeTag Margin0">Add To Collection</p>
                        {
                            status==="Old" && (
                                <div>
                                    <div className="flex center">
                                        <p className="Section Underline">Existing</p>
                                        <p onClick={()=>setStatus("New")} className="marginL Section">New</p>
                                    </div>
                                    <p className="Section Margin0">Choose Collections</p>
                                    <div className="flex collectionWrapper">
                                    {
                                        Collection.map((post)=>{
                                            if(!animeCol.includes(post)){
                                                return(
                                                <div className="formWrapper">
                                                    <input className="margin0 Section" onChange={()=>handleSelectedChange(post)} type="checkbox" id={post} name={post} value={post}/>
                                                    <label className="break Section margin0" for={post}> {post}</label><br></br>
                                                </div>
                                                )}
                                            return(
                                                <div>
                                                </div>
                                            )
                                        })
                                    }
                                    </div>
                                    {
                                        val3 && (
                                            <p className="error">Choose the collections!</p>
                                        )
                                    }
                                    <button onClick={()=>addToExistingCollection()} className="button">Add</button>
                                </div>
                            )
                        }
                        {
                            status==="New" && (
                                <div>
                                    <div className="flex center">
                                        <p onClick={()=>setStatus("Old")} className="Section">Existing</p>
                                        <p className="marginL Section Underline">New</p>
                                    </div>
                                    <p className="Section Margin0">Collection Name</p>
                                    <input type="text" value={nama} onChange={e => setNama(e.target.value)} className='form' placeholder="ex: Favorite List"></input>
                                    {
                                        val1 && (
                                            <p className="error">Collection Existed</p>
                                        )
                                    }
                                    {
                                        val2 && (
                                            <p className="error">Cannot Use Special Character</p>
                                        )
                                    }
                                    {
                                        val4 && (
                                            <p className="error">Name cannot be empty</p>
                                        )
                                    }
                                    <button onClick={()=>addNewCollection(nama)} className="button">Add</button>
                                </div>
                            )
                        }
                </div>
            </div>
        </Content>
    )
}