/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Content } from './styles';
import star from '../../assets/star.svg'
import parse from 'html-react-parser';
import bookmark from '../../assets/bookmark.svg'
import { AddToCollection } from "../AddToCollection/AddToCollection";

export const AnimeDetail = ({data, fungsi, fungsi2}) =>{
    const monthName = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    const [showAddTo, setShowAddTo] = useState(false)
    const [animeCol, setAnimeCol] = useState([])
    useEffect(()=>{
        if((localStorage.getItem(data.title.romaji)===null) || (localStorage.getItem(data.title.romaji)==='') ){
            localStorage.setItem(data.title.romaji,JSON.stringify([]))
        }else{
            setAnimeCol(JSON.parse(localStorage.getItem(data.title.romaji)))
        }
    },[])


    return(
        <Content>
            <div className="Container">
                {data !== undefined && (
                    <div>
                        <img onClick={()=>setShowAddTo(true)} alt="icon" className="bookmark" src={bookmark} />
                        <img alt="banner" className="Banner" src={data.bannerImage}/>
                        <div className="DetailWrapper flex">
                            <img className="AnimeImage" alt="cover" src={data.coverImage.extraLarge}></img>
                            <div className="DetailAnime">
                                <p className="title">{data.title.romaji}</p>
                                <div className="Rating flex">
                                    <p className="AnimeRating">Rating: </p>
                                    <img alt="icon" src={star} className="IconRating"></img>
                                    <p className="AnimeRating">{data.averageScore}</p>
                                </div>
                                <p className="AnimeRating">Total Episodes: {data.episodes}</p>
                            </div>
                        </div>
                        <div className="DetailWrapper2">
                            <p className="AnimeTag">Airing Detail</p>
                            <p className="AnimeRating">Status: {data.status}</p>
                            <p className="AnimeRating">Start Date: {data.startDate.day} {monthName[data.startDate.month - 1]} {data.startDate.year}</p>
                            <p className="AnimeRating">End Date: {data.endDate.day} {monthName[data.endDate.month - 1]} {data.endDate.year}</p>
                            <p className="AnimeTag">Genres</p>
                                <p className="AnimeRating">
                                {
                                    data.genres.map((post) => {
                                        if(post===data.genres[0]){
                                            return(
                                                <span>{post}</span>
                                            )
                                        }else{
                                            return(
                                                <span>, {post}</span>
                                            )
                                        }
                                    })
                                }</p>
                            <p className="AnimeTag">Description</p>
                            <p className="AnimeRating">{parse(data.description)}</p>
                            <p className="AnimeTag">Collected At</p>
                            {
                                animeCol.map((post)=>(
                                    <p onClick={()=>{fungsi();fungsi2(post)}} className="AnimeRating AnimeCollection">{post}</p>
                                ))
                            }
                        </div>
                    </div>
                )}

            {
                    showAddTo && (
                    <AddToCollection fungsi={()=>setShowAddTo(false)} animeCol={animeCol} data={data}/>
                    )
                }
                
            </div>
        </Content>
    )
}