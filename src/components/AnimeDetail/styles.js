import styled from '@emotion/styled';

export const Content = styled.div`
@media screen {
    word-break: break-word; 

    .Container{
        background-color:#212121;
        padding-bottom:20vh;
    }

    .bookmark{
        position: fixed;
        bottom:0;
        cursor:pointer;
        right:0;
        margin-right:calc(20px + 1vw);
        margin-bottom:36px;
        height:64px;
    }

    .Banner{
        width: -webkit-fill-available;
        width: -moz-available;
        max-height:30vh;
        object-fit:cover;
    }

    .flex{
        display:flex
    }

    .DetailWrapper{
        margin-top:80px;
        width:88%;
        margin-left:auto;
        margin-right:auto;
    }

    .DetailWrapper2{
        margin-top:24px;
        width:88%;
        margin-left:auto;
        margin-right:auto;
        text-align:justify;
    }

    .AnimeImage{
        width:20%;
        height: max-content;
    }

    .DetailAnime{
        flex:70%;
        padding-left:32px;
    }

    .title{
        font-weight:bold;
        font-size:48px;
        margin:0;
    }

    .Rating{
        align-items:center;
    }

    .AnimeRating{
        font-size:18px;
        margin:0;
        margin-top:8px;
    }

    .AnimeCollection{
        cursor:pointer;
        color: #FCCC24;
        text-decoration:underline;
    }

    .IconRating{
        height:16px;
        margin-left:8px;
        margin-right:4px;
        margin-top:8px;
    }

    .AnimeTag{
        font-weight:bold;
        font-size:28px;
        color: #166DC4;
    }
}

@media screen and (max-width: 1000px) {
    .AnimeImage{
        width:30%
    }

    .DetailAnime{
        padding-left:16px;
    }

    .DetailWrapper{
        margin-top:32px;
    }

    .DetailWrapper2{
        margin-top:12px;
    }

    .title{
        font-size:24px;
    }

    .AnimeRating{
        font-size:14px;
    }

    .AnimeTag{
        font-size:21px;
    }
} 
`;
