import React from "react";
import {
    useQuery,
    gql
} from "@apollo/client";
import { Content } from './styles';
import star from '../../assets/star.svg'



export const AnimeList = ({page, lihatDetail, simpanData}) =>{
  
    const { loading, error, data } = useQuery(gql`
    {
        Page (page: ${page}, perPage: 10) {
          pageInfo {
            total
            currentPage
            lastPage
            hasNextPage
            perPage
          }
          media (type:ANIME, sort:SCORE_DESC) {
            id
            bannerImage
            title {
            romaji
            }
            coverImage {
            extraLarge
            large
            medium
            color
            }
            averageScore
            description
            episodes
            genres
            status
            startDate {
                year
                month
                day
            }
            endDate {
                year
                month
                day
            }           
          }
        }
      }
    `);
    if (loading) return <Content><p className="loading">Loading...</p></Content>;
    if (error) return <Content><p className="loading">Error :(</p></Content>;
  
    return  (
        <Content>
            <div className="AnimeList flex">
                {
                data.Page.media.map((post) =>{
                    
                if(post===data.Page.media[0]){
                    return(
                        <div className="AnimeContainer" key={post.id}>
                            <div onClick={()=>{lihatDetail();simpanData(post)}} className="AnimeCard flex">
                                <img className="banner" src={post.coverImage.medium} alt="banner"></img>
                                <div className="AnimeDetail">
                                    <p className="AnimeTitle">{post.title.romaji}</p>
                                    <div className="Rating flex">
                                        <p className="AnimeRating">rating: </p>
                                        <img alt="icon" src={star} className="IconRating"></img>
                                        <p className="AnimeRating">{post.averageScore}</p>
                                    </div>
                                    <p className="SeeDetail">See Detail   →</p>
                                </div>
                            </div>
                        </div>
                    )
                }else{
                    return(
                        <div className="AnimeContainer" key={post.id}>
                            <div className="separator"/>
                            <div onClick={()=>{lihatDetail();simpanData(post)}} className="AnimeCard flex">
                                <img className="banner" src={post.coverImage.medium} alt="banner"></img>
                                <div className="AnimeDetail">
                                    <p className="AnimeTitle">{post.title.romaji}</p>
                                    <div className="Rating flex">
                                        <p className="AnimeRating">rating: </p>
                                        <img alt="icon" src={star} className="IconRating"></img>
                                        <p className="AnimeRating">{post.averageScore}</p>
                                    </div>
                                    <p className="SeeDetail">See Detail   →</p>
                                </div>
                            </div>
                        </div>
                    )
                }
                    
                })
                }
            </div>
      </Content>
    );
  }