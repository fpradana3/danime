import styled from '@emotion/styled';

export const Content = styled.div`
@media screen {
    word-break: break-word; 

    .banner{
        width:100px;
        height:fit-content;
    }

    .AnimeContainer{
        flex:50%;
    }

    .AnimeList{
        width:88%;
        margin-left:auto;
        margin-right:auto;
        flex-wrap: wrap;
        background-color:#212121;
    }

    .flex{
        display:flex
    }

    .AnimeCard{
        margin:8px;
        padding:16px;
        cursor:pointer;
    }

    .separator{
        height:1px;
        width:100%;
        background-color:white;
    }

    .AnimeContainer:nth-child(2) > .separator{
        display:none;
    }

    .AnimeTitle{
        font-size:24px;
        margin:0;
        transition: color 0.3s;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        overflow: hidden;
    }

    .AnimeCard:hover .AnimeTitle{
        color:#FCCC24;
    }

    .SeeDetail{
        color:#FCCC24;
        position:absolute;
        bottom:0;
        right:0;
        margin:0;
    }

    .Rating{
        align-items:center;
    }

    .AnimeRating{
        font-size:18px;
        margin:0;
        margin-top:8px;
    }

    .IconRating{
        height:16px;
        width:fit-content;
        margin-left:8px;
        margin-right:4px;
        margin-top:8px;
    }

    .AnimeDetail{
        margin-left:24px;
        width: -webkit-fill-available;
        width: -moz-available;
        position:relative;
    }
    
    .loading{
        position:absolute;
        top:60%;
        left:50%;
        transform:translate(-50%, -50%);
        height:120px;
    }
}

@media screen and (max-width: 1000px) {
    .AnimeRating{
        font-size:14px;
    }

    .AnimeContainer{
        flex:100%;
    }

    .AnimeContainer:nth-child(2) > .separator{
        display:block;
    }
} 
`;
