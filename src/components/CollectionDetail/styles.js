import styled from '@emotion/styled';

export const Content = styled.div`
@media screen {
    word-break: break-word; 

    .banner{
        width:100px;
        height:fit-content;
    }

    .AnimeContainer{
        flex:50%;
        max-width:50%;
    }

    .AnimeList{
        width:88%;
        margin-left:auto;
        margin-right:auto;
        background-color:#212121;
        position:relative;
    }

    .wrap{
        flex-wrap: wrap;
    }

    .flex{
        display:flex
    }

    .AnimeCard{
        margin:8px;
        padding:16px;
        cursor:pointer;
    }

    .separator{
        height:1px;
        width:100%;
        background-color:white;
    }

    .AnimeContainer:nth-child(2) > .separator{
        display:none;
    }

    .AnimeTag{
        font-weight:bold;
        font-size:28px;
        color: #166DC4;
    }

    .AnimeTitle{
        font-size:24px;
        margin:0;
        transition: color 0.3s;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        overflow: hidden;
    }

    .AnimeCard:hover .AnimeTitle{
        color:#FCCC24;
    }

    .SeeDetail{
        color:#FCCC24;
        position:absolute;
        bottom:0;
        left:0;
        margin:0;
    }

    .trash{
        position:absolute;
        bottom:0;
        right:0;
        height:32px;
        margin:0;
    }

    .form{
        width: -webkit-fill-available;
        width: -moz-available;
        padding: 15px;
        border-radius: 15px;
        outline: none;
        margin-top:8px;
        margin-bottom: 8px;
        border: 1px solid #6F6F6F;
        overflow: hidden;  
        text-overflow:ellipsis;
        white-space: nowrap;
    }

    .Rating{
        align-items:center;
    }

    .center{
        margin-left:auto;
        margin-right:auto;
        justify-content:center;
        text-align:center;
    }

    .AnimeRating{
        font-size:18px;
        margin:0;
        margin-top:8px;
    }

    .IconRating{
        height:16px;
        width:fit-content;
        margin-left:8px;
        margin-right:4px;
        margin-top:8px;
    }

    .AnimeDetail{
        margin-left:24px;
        width: -webkit-fill-available;
        width: -moz-available;
        position:relative;
    }
    
    .loading{
        position:absolute;
        top:50%;
        left:50%;
        transform:translate(-50%, -50%);
        height:120px;
    }

    .outside{
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        opacity: 0.6;
        align-items: center;
        justify-content: center;
        height: 100vh;
        width: 100vw;
        z-index: 100;
    }

    .error{
        color:#FF7D7D;
        text-align:right;
        margin:0;
    }

    .Modal{
        position: fixed;
        min-width: 60vw;
        z-index: 120;
        background-color: white;
        top: 50%;
        border-radius: 15px;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 5vh 5vw 5vh 5vw;
        max-height: 70vh;
        color:black;
        overflow: scroll;
    }

    .textkosong{
        margin-top:64px;
        text-align:center;
    }

    .Modal::-webkit-scrollbar{
        width:0;
        height:0;
        scrollbar-width: none;
    }

    .Margin0{
        margin:0
    }

    .Section{
        font-size:24px;
        cursor:pointer;
        margin-top:24px;
    }

    .button{
        background-color: #FCCC24;
        border: none;
        font-weight:bold;
        padding: 15px;
        width: 60%;
        margin-bottom: 36px;
        cursor: pointer;
        display: block;
        border-radius: 15px;
        margin-left: auto;
        margin-right: auto;
    }
    .margint{
        margin-top:24px;
    }

    .NamaKoleksi{
        font-weight:bold;
        font-size:48px;
        margin:0;
        color: #FCCC24;
        padding-top:36px;
        margin-bottom:24px;
    }

    .edit{
        height:36px;
        position:absolute;
        top:0;
        right:0;
        margin-top:52px;
    }
}

@media screen and (max-width: 1000px) {
    .AnimeRating{
        font-size:14px;
    }

    .Section{
        font-size:16px;
    }

    .NamaKoleksi{
        font-size:24px;
    }


    .AnimeContainer{
        flex:100%;
        max-width:100%
    }

    .AnimeTag{
        font-size:21px;
    }

    .trash{
        height:24px;
    }

    .edit{
        height:24px;
        margin-top:36px;
    }

    .AnimeContainer:nth-child(2) > .separator{
        display:block;
    }

    .form{
        padding:12px;
    }
} 
`;
