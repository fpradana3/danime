import React, { useState } from "react";
import { Content } from './styles';
import star from '../../assets/star.svg'
import trash from '../../assets/trash.svg'
import edit from '../../assets/edit.svg'



export const CollectionDetail = ({namaKoleksi, fungsi1, fungsi2, fungsi3}) =>{
    const [dataKoleksi, setDataKoleksi] = useState(JSON.parse(localStorage.getItem(namaKoleksi)))
    const [showremove, setShowremove] = useState(false)
    const [untukDihapus, setUntukDihapus] = useState('')
    const [dataAnime, setDataAnime] = useState('')

    const openConfirm = async (e) =>{
        setUntukDihapus(e.title.romaji)
        setShowremove(true)
    }

    const removeAnime = () =>{
        var array2 = JSON.parse(localStorage.getItem(namaKoleksi))
        array2.splice(dataAnime, 1);
        console.log(array2)
        setDataKoleksi(array2)
        localStorage.setItem(namaKoleksi,JSON.stringify(array2))
        var untukDihapus1 = JSON.parse(localStorage.getItem(untukDihapus))
        for(let i=0;i<untukDihapus1.length;i++){
            var index2 = untukDihapus1.indexOf(namaKoleksi);
            console.log("kalo ini bener dong")
            console.log(index2)
            untukDihapus1.splice(index2, 1)
            localStorage.setItem(untukDihapus,JSON.stringify(untukDihapus1))
        }
        setShowremove(false)
    }

    const [nama,setNama] = useState("")
    const [val1, setval1] = useState(false)
    const [val2, setval2] = useState(false)
    const [val3, setval3] = useState(false)
    const [val4,setval4] = useState(false)
    const [showEditCol, setShowEditCol] = useState(false)

    const reset = () =>{
        setNama("")
        setval1(false)
        setval2(false)
        setval3(false)
        setval4(false)
    }

    const changeCollectionName = async (namanya) => {
        if(namanya===""){
            setval3(true)
            return
        }else{
            setval3(false)
        }
        if((namanya.replace(/\s/g,'')).replace(/^[a-z\d.]*$/i,"")!==""){
            setval2(true)
            return
        }else{
            setval2(false)
        }if(namanya===namaKoleksi){
            setval4(true)
            return
        }else{
            setval4(false)
        }
        var isi = JSON.parse(localStorage.getItem(namanya))
        if(isi===null){
            isi = JSON.parse(localStorage.getItem(namaKoleksi))
            localStorage.setItem(namanya,JSON.stringify(isi))
            var array2 = JSON.parse(localStorage.getItem("MyCollections"))
            var index = array2.indexOf(namaKoleksi);
            array2.splice(index, 1);
            array2.unshift(namanya)
            localStorage.setItem("MyCollections",JSON.stringify(array2))
            localStorage.removeItem(namaKoleksi)
            for(let i=0;i<isi.length;i++){
                var arraynime = JSON.parse(localStorage.getItem(isi[i].title.romaji))
                var index2 = arraynime.indexOf(namaKoleksi);
                arraynime.splice(index2, 1)
                arraynime.unshift(namanya)
                localStorage.setItem(isi[i].title.romaji,JSON.stringify(arraynime))
            }
            
            setShowEditCol(false)
            fungsi3(namanya)
            reset()
        }else{
            setval1(true)
        }
    }

    return  (
        <Content>
            <div>
                {
                    dataKoleksi.length!==0?
                    <div className="AnimeList">
                        <p className="NamaKoleksi center">{namaKoleksi}</p>
                        <img onClick={()=>setShowEditCol(true)} alt="icon" src={edit} className="edit"/>
                        <div className="flex wrap">
                        {
                            dataKoleksi.map((post,keberapa)=>{
                                if(post===dataKoleksi[0]){
                                    return(
                                        <div className="AnimeContainer" key={post.id}>
                                            <div onClick={e=>{e.stopPropagation();fungsi1();fungsi2(post)}} className="AnimeCard flex">
                                                <img className="banner" src={post.coverImage.medium} alt="banner"></img>
                                                <div className="AnimeDetail">
                                                    <p className="AnimeTitle">{post.title.romaji}</p>
                                                    <div className="Rating flex">
                                                        <p className="AnimeRating">rating: </p>
                                                        <img alt="icon" src={star} className="IconRating"></img>
                                                        <p className="AnimeRating">{post.averageScore}</p>
                                                    </div>
                                                    <p className="SeeDetail">See Detail   →</p>
                                                    <img onClick={e=>{e.preventDefault();e.stopPropagation();openConfirm(post);setDataAnime(keberapa)}} alt="icon" className="trash" src={trash} />
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }else{
                                    return(
                                        <div className="AnimeContainer" key={post.id}>
                                            <div className="separator"/>
                                            <div  onClick={e=>{e.stopPropagation();fungsi1();fungsi2(post)}} className="AnimeCard flex">
                                                <img className="banner" src={post.coverImage.medium} alt="banner"></img>
                                                <div className="AnimeDetail">
                                                    <p className="AnimeTitle">{post.title.romaji}</p>
                                                    <div className="Rating flex">
                                                        <p className="AnimeRating">rating: </p>
                                                        <img alt="icon" src={star} className="IconRating"></img>
                                                        <p className="AnimeRating">{post.averageScore}</p>
                                                    </div>
                                                    <p className="SeeDetail">See Detail   →</p>
                                                    <img onClick={e=>{e.preventDefault();e.stopPropagation();openConfirm(post);setDataAnime(keberapa)}} alt="icon" className="trash" src={trash} />
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                            })
                        }
                        </div>
                    </div>
                    :
                    <div  className="AnimeList">
                        <p className="NamaKoleksi center">{namaKoleksi}</p>
                        <img onClick={()=>setShowEditCol(true)} alt="icon" src={edit} className="edit"/>
                        <p className="textkosong">data kosong</p>
                    </div>
                }
                 {
                    showremove && (
                        <div>
                            <div onClick={()=>setShowremove(false)} className="outside"></div>
                            <div className="Modal">
                                <p className="flex center AnimeTag Margin0">Remove Confirmation</p>
                                <p className="Section center Margin0">Anda yakin ingin menghapus anime ini dari koleksi anda?</p>
                                <button onClick={()=>removeAnime()} className="button margint">Hapus</button>
                            </div>
                        </div>
                    )
                }

                {
                    showEditCol && (
                        <div>
                            <div onClick={()=>{setShowEditCol(false);reset()}} className="outside"></div>
                            <div className="Modal">
                                <p className="flex center AnimeTag Margin0">Change Collection Name</p>
                                <p className="Section Margin0">Collection Name</p>
                                <input type="text" value={nama} onChange={e => setNama(e.target.value)} className='form' placeholder="ex: Favorite List"></input>
                                {
                                    val1 && (
                                        <p className="error">Collection Existed</p>
                                    )
                                }
                                {
                                    val2 && (
                                        <p className="error">Cannot Use Special Character</p>
                                    )
                                }
                                {
                                    val3 && (
                                        <p className="error">Name cannot be empty</p>
                                    )
                                }

                                {
                                    val4 && (
                                        <p className="error">Name cannot be same</p>
                                    )
                                }
                                <button onClick={()=>changeCollectionName(nama)} className="button margint">Confirm</button>
                            </div>
                        </div>
                    )
                }
            </div>
      </Content>
    );
  }